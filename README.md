This was a group project for an itch.io game jam called Finish It.

The team consisted of 3 members:

	#Member 1 - Took care of the music and the AI 

	#Member 2 - Took care of the level design

	#Member 3 (Me) - Took care of the scripting of the game
	

The game could not be finished in time, and ended up being scrapped. 
Link to the game can be found [here](https://spikyphrog.itch.io/finishitlight)